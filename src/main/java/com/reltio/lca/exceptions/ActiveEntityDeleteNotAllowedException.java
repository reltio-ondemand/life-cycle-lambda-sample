package com.reltio.lca.exceptions;

public class ActiveEntityDeleteNotAllowedException extends RuntimeException {

    public ActiveEntityDeleteNotAllowedException(String message) {
        super(message);
    }
}
