package com.reltio.lca.exceptions;

public class CountryNotMatchingException extends RuntimeException {

    public CountryNotMatchingException(String message) {
        super(message);
    }
}
