package com.reltio.lca.entities;

import com.reltio.lifecycle.framework.*;
import com.reltio.lifecycle.lambda.LifeCycleActionHandler;

import java.util.Map;
import java.util.Optional;

/**
 * This is the reference example on the hook 'beforeReferenceAttributeAdded' to show how relation attributes
 * ,start object and end objects details can be accessed in the LCA
 */
public class BeforeReferenceAttributeAddedEntityAction extends LifeCycleActionHandler {

    @Override
    public void beforeReferenceAttributeAdded(IReltioAPI reltioAPI, ILifeCycleReferenceAttributeData data) {
        IAttributes attributes = data.getRelation().getAttributes();
        String addressType = getAttribute("AddressType", attributes);
        Map relation = (Map) data.toMap().get("relation");
        Map startObject = (Map) relation.get("startObject");
        Map endObject = (Map) relation.get("endObject");
        reltioAPI.logInfo("Reference getting added = " + data.getAttributeName() + ", relation type = " + data.getRelation().getType() + ", addressType attribute value = " + addressType);
        reltioAPI.logInfo("start object uri = " + startObject.get("objectURI") + ", end object uri = " + endObject.get("objectURI"));
    }

    private String getAttribute(String attributeName, IAttributes attributes) {
        String value = null;
        for (IAttributeValue attribute : attributes.getAttributeValues(attributeName)) {
            if (attribute.isOv()) {
                value = (String) attribute.getValue();
                break;
            }
        }
        return value;
    }
}
