package com.reltio.lca.entities;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.reltio.lca.exceptions.CountryNotMatchingException;
import com.reltio.lifecycle.framework.*;
import com.reltio.lifecycle.lambda.LifeCycleActionHandler;

import java.util.*;

/**
 * This LCA example shows how to get the attributes of the winner and looser details
 */
public class BeforeMergeEntityAction extends LifeCycleActionHandler {

    private static final String ENTITIES_BY_URIS = "entities/_byUris";
    private static final String COUNTRY = "Country";
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public ILifeCycleMergeData beforeMerge(IReltioAPI api, ILifeCycleMergeData data) {
        String country = null;
        List<Map> entitiesMap;
        try {
            Map<String, Object> request = new HashMap() {{
                put("select", "uri,type,attributes.Country");
                put("uris", data.getLoserURIs());
                put("options", "ovOnly");
            }};
            String entities = api.post(ENTITIES_BY_URIS, objectMapper.writeValueAsString(request), Collections.emptyMap());
            entitiesMap = objectMapper.readValue(entities, List.class);
            List<IAttributeValue> countryAttribUte = data.getWinner().getAttributes().getAttributeValues("Country");
            country = (String) countryAttribUte.get(0).getValue();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        validateCountry(entitiesMap, country);
        return data;
    }

    private void validateCountry(List<Map> entitiesMap, String winnerCountry) {
        for (Map entity : entitiesMap) {
            if (!winnerCountry.equals(getAttribute(entity))) {
                throw new CountryNotMatchingException("Country is not matching between the entities");
            }
        }
    }

    private String getAttribute(Map<String, Object> entity) {
       String country = null;
        List<Map> attributes = (List<Map>) ((Map)entity.get("attributes")).get(COUNTRY);
        if (attributes != null) {
            country = (String)attributes.get(0).get("value");
        }
        return country;
    }
}
