package com.reltio.lca.entities;

import com.reltio.lifecycle.framework.*;
import com.reltio.lifecycle.lambda.LifeCycleActionHandler;

import java.util.List;

/**
 * This is the Example of the LCA how to create new attribute in LCA and adding to specific crosswalk
 * Recommended to use Full Name Cleanser (https://docs.reltio.com/datacleanse/cleanselibfullname.html) for better performance instead of Life Cycle Action for this case.
 */
public class BeforeSaveEntityAction extends LifeCycleActionHandler {

    @Override
    public ILifeCycleObjectData beforeSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
        IAttributes attributes = data.getObject().getAttributes();
        List<IAttributeValue> fullNames = attributes.getAttributeValues("Name");
        if (fullNames.isEmpty()) {
            List<IAttributeValue> firstNames = attributes.getAttributeValues("FirstName");
            List<IAttributeValue> lastNames = attributes.getAttributeValues("LastName");
            if (!firstNames.isEmpty() && !lastNames.isEmpty()) {
                ISimpleAttributeValue firstName = (ISimpleAttributeValue) firstNames.get(0);
                ISimpleAttributeValue lastName = (ISimpleAttributeValue) lastNames.get(0);
                String firstNameValue = firstName.getStringValue();
                String lastNameValue = lastName.getStringValue();
                String fullName = firstNameValue + " " + lastNameValue;
                ISimpleAttributeValue newValue = attributes.createSimpleAttributeValue("Name").value(fullName).build();
                attributes.addAttributeValue(newValue);
                //Here we are adding address type to first crosswalk but customer need to know which crosswalk they want to add,
                // and respectively they should be adding
                data.getObject().getCrosswalks().getCrosswalks().get(0).getAttributes().addAttribute(newValue);
                return data;
            }
        }
        return null;
    }
}
