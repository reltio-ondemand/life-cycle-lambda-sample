package com.reltio.lca.entities;

import com.reltio.lca.exceptions.ActiveEntityDeleteNotAllowedException;
import com.reltio.lifecycle.framework.IAttributeValue;
import com.reltio.lifecycle.framework.ILifeCycleObjectData;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.lambda.LifeCycleActionHandler;

/**
 * This LCA will stop entity being deleted if the attribute 'Status' is 'LIVE'
 */
public class BeforeDeleteEntityAction extends LifeCycleActionHandler {

    @Override
    public ILifeCycleObjectData beforeDelete(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
        data.getObject().getAttributes().getAttributeValues("Status").stream().filter(IAttributeValue::isOv)
                .filter(iAttributeValue -> "Live".equals(iAttributeValue.getValue())).findAny().ifPresent(attribute -> {
            throw new ActiveEntityDeleteNotAllowedException("Live entities cannot be deleted");
        });
        return data;
    }
}