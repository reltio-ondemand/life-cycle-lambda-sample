package com.reltio.lca.entities;

import com.reltio.lifecycle.framework.IAttributeValue;
import com.reltio.lifecycle.framework.ILifeCycleValidationData;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.framework.validation.ValidationError;
import com.reltio.lifecycle.lambda.LifeCycleActionHandler;

import java.util.List;

/**
 * This is the reference example on the hook 'validate' to show how to validate the entity and add the validation for an attribute
 * Please be noted that, in validate hook you should always return the data (ILifeCycleValidationData)
 * */
public class ValidateEntityAction extends LifeCycleActionHandler {

    @Override
    public ILifeCycleValidationData validate(IReltioAPI reltioAPI, ILifeCycleValidationData data) {
        List<IAttributeValue> firstNamesAttribute = data.getObject().getAttributes().getAttributeValues("FirstName");
        if (firstNamesAttribute != null && !firstNamesAttribute.isEmpty()) {
            String nameValue = (String) firstNamesAttribute.get(0).getValue();
            if (nameValue.startsWith("#")) {
                data.addValidationError(ValidationError.incorrectAttribute(firstNamesAttribute.get(0), "FirstName cannot start with the #"));
            }
        }
        List<IAttributeValue> lastNames = data.getObject().getAttributes().getAttributeValues("LastName");
        if (lastNames != null && !lastNames.isEmpty()) {
            String nameValue = (String) lastNames.get(0).getValue();
            if (nameValue.startsWith("#")) {
                data.addValidationError(ValidationError.incorrectAttribute(lastNames.get(0), "LastName cannot start with the #"));
            }
        }
        return data;
    }
}