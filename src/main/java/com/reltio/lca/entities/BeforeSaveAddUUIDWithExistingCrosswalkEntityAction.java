package com.reltio.lca.entities;

import com.reltio.lifecycle.framework.*;
import com.reltio.lifecycle.lambda.LifeCycleActionHandler;

import java.util.Optional;
import java.util.UUID;

/**
 * To Add the unique Attribute values to crosswalks of entity we recommend using the Auto generators
 */
public class BeforeSaveAddUUIDWithExistingCrosswalkEntityAction extends LifeCycleActionHandler {

    @Override
    public ILifeCycleObjectData beforeSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
        IObject object = data.getObject();
        IAttributes attributes = object.getAttributes();
        String uniqueID = getAttribute("UniqueID", attributes);
        if (uniqueID == null) {
            ISimpleAttributeValue attribute = attributes.createSimpleAttributeValue("UniqueID").value(UUID.randomUUID().toString()).build();
            attributes.addAttributeValue(attribute);
            ICrosswalk uniqueCrosswalk = data.getObject().getCrosswalks().getCrosswalks().get(0);
            uniqueCrosswalk.getAttributes().addAttribute(attribute);
        }
        return data;
    }

    private String getAttribute(String attributeName, IAttributes attributes) {
        Optional<IAttributeValue> optionalAttribute = attributes.getAttributeValues(attributeName).stream().filter(IAttributeValue::isOv).findFirst();
        return optionalAttribute.isPresent() ? (String) optionalAttribute.get().getValue() : null;
    }
}
