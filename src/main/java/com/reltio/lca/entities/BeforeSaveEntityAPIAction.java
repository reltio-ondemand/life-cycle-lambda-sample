package com.reltio.lca.entities;

import com.reltio.lifecycle.framework.IAttributeValue;
import com.reltio.lifecycle.framework.ILifeCycleObjectData;
import com.reltio.lifecycle.framework.IObject;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.lambda.LifeCycleActionHandler;

import java.util.List;

public class BeforeSaveEntityAPIAction extends LifeCycleActionHandler {

    @Override
    public ILifeCycleObjectData beforeSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
        //With below api call it gets the entity details with id 123
        IObject object = reltioAPI.getObject("entities/123");
        List<IAttributeValue> firstNames =  object.getAttributes().getAttributeValues("FirstName");
        String firstName = (String) firstNames.get(0).getValue();
        return data;
    }
}
