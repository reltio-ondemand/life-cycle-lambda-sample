package com.reltio.lca.entities;

import com.reltio.lifecycle.framework.*;
import com.reltio.lifecycle.lambda.LifeCycleActionHandler;

import java.util.List;
import java.util.Optional;

/**
 * This LCA handler wll add the AddressType under Addresses nested attribute if missing AddressType attribute
 */
public class BeforeSaveNestedAttributeEntityAction extends LifeCycleActionHandler {

    @Override
    public ILifeCycleObjectData beforeSave(final IReltioAPI api, final ILifeCycleObjectData data) {

        final IAttributes attributes = data.getObject().getAttributes();
        List<IAttributeValue> addresses = attributes.getAttributeValues("Addresses");
        addresses.forEach(nestedAttributeObject -> {
            IAttributes addressValues = (IAttributes) nestedAttributeObject.getValue();
            List<IAttributeValue> attributeValues = addressValues.getAllAttributeValues();
            Optional addressType = attributeValues.stream().filter(attributeValue -> attributeValue.getType().contains("AddressType")).findAny();
            if (!addressType.isPresent()) {
                ISimpleAttributeValue addressTypeAttribute = addressValues.createSimpleAttributeValue("AddressType").value("Office").build();
                api.logInfo(String.format("Adding address type = %s, value = %s", addressTypeAttribute.getUri(), addressTypeAttribute.getValue()));
                addressValues.addAttributeValue(addressTypeAttribute);
                //Here we are adding address type to first crosswalk but customer need to know which crosswalk they want to add
                // and respectively they should be adding
                data.getObject().getCrosswalks().getCrosswalks().get(0).getAttributes().addAttribute(addressTypeAttribute);
            }
        });
        return data;
    }
}
