package com.reltio.lca.relations;

import com.reltio.lifecycle.framework.*;
import com.reltio.lifecycle.lambda.LifeCycleActionHandler;

import java.util.List;
import java.util.Map;

/**
 * This is the example of which defaulting 'AddressType' attribute to 'Physical'
 */
public class BeforeSaveRelationAction extends LifeCycleActionHandler {

    @Override
    public ILifeCycleObjectData beforeSave(IReltioAPI reltioAPI, ILifeCycleObjectData data) {
        IAttributes attributes = data.getObject().getAttributes();
        List<IAttributeValue> addressType = attributes.getAttributeValues("AddressType");

        if (addressType == null || addressType.size() == 0) {
            ISimpleAttributeValue newValue = attributes.createSimpleAttributeValue("AddressType").value("Physical").build();
            data.getObject().getCrosswalks().getCrosswalks().get(0).getAttributes().addAttribute(newValue);
            attributes.addAttributeValue(newValue);

            Map relation = (Map) data.toMap().get("object");
            Map startObject = (Map) relation.get("startObject");
            Map endObject = (Map) relation.get("endObject");
            reltioAPI.logInfo("Relation type = "
                    + data.getObject().getType() + ", addressType attribute value = " + addressType);
            reltioAPI.logInfo("start object uri = " + startObject.get("objectURI") + ", end object uri = " + endObject.get("objectURI"));
            return data;
        }

        return null;
    }
}
