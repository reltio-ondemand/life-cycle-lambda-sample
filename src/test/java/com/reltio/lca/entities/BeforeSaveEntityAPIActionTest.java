package com.reltio.lca.entities;

import com.reltio.lca.BaseTest;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.server.core.framework.ReltioAPI;
import com.reltio.lifecycle.test.LifecycleExecutor;
import com.reltio.microservices.lcaservice.service.services.LifeCycleHook;
import org.junit.Ignore;
import org.junit.jupiter.api.Test;


public class BeforeSaveEntityAPIActionTest extends BaseTest {


    @Test
    @Ignore
    public void testRestCallToTenant() throws Exception {
        LifecycleExecutor executor = new LifecycleExecutor();
        BeforeSaveEntityAPIAction handler = new BeforeSaveEntityAPIAction();
        String access_token = "8a324330-cfe1-48b9-866f-a137afe86700";
        //Appropriate details need to pass here environmental details, tenantid and the active access token generated from https://auth.reltio.com/oauth/token,
        //with the reltioAPI object passed to the executor same instance will be used for making any rest api calls to reltio tenant with the access token passed
        //when in deployed into aws lambda reltio server will pass the active token and needs to no change in the actual LCA handler this is only for the testing where LCA
        //makes rest api calls to reltio tenant
        //when referring this test case please remove the @Ignore annotation in order to run the test case
        IReltioAPI reltioAPI = new ReltioAPI("https://test.reltio.com/reltio", "testTenantId", access_token);
        String input = readResource("/entities/beforesave/input.json");
        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, reltioAPI, input);
    }
}
