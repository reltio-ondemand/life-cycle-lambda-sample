package com.reltio.lca.entities;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.reltio.lca.BaseTest;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.test.LifecycleExecutor;
import com.reltio.microservices.lcaservice.service.services.LifeCycleHook;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertNotNull;


public class BeforeSaveNestedAttributeEntityActionTest extends BaseTest {

    private final LifecycleExecutor actionExecutor = new LifecycleExecutor();
    private final BeforeSaveNestedAttributeEntityAction action = new BeforeSaveNestedAttributeEntityAction();
    private ObjectMapper objectMapper = new ObjectMapper();
    private IReltioAPI reltioAPI;

    @BeforeEach
    public void setupMocks() {
        reltioAPI = mockReltioAPI();
    }

    @Test
    public void testDefaultAddressType() throws Exception {
        String input = readResource("/entities/beforesave/nestedinput-1.json");
        String result = actionExecutor.executeAction(action, LifeCycleHook.beforeSave, input);
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        ArrayNode addressNode = (ArrayNode) jsonNode.get("object").get("attributes").get("Addresses");
        addressNode.forEach(address -> {
            JsonNode jsonAddressTypeNode = address.get("value").get("AddressType");
            assertNotNull(jsonAddressTypeNode.get(0).get("value"));
        });
    }
}