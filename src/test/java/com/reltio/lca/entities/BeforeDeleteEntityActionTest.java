package com.reltio.lca.entities;

import com.reltio.lca.BaseTest;
import com.reltio.lca.exceptions.ActiveEntityDeleteNotAllowedException;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.test.LifecycleExecutor;
import com.reltio.microservices.lcaservice.service.services.LifeCycleHook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BeforeDeleteEntityActionTest extends BaseTest {

    private final LifecycleExecutor executor = new LifecycleExecutor();
    private final BeforeDeleteEntityAction action = new BeforeDeleteEntityAction();
    private IReltioAPI reltioAPI;

    @BeforeEach
    public void setupMocks() {
        reltioAPI = mockReltioAPI();
    }

    @Test
    public void beforeDeleteReject() throws Exception {
        String input = readResource("/entities/beforedelete/beforedelete-reject.json");
        Assertions.assertThrows(ActiveEntityDeleteNotAllowedException.class, () -> executor.executeAction(action, LifeCycleHook.beforeDelete, reltioAPI, input));
    }

    @Test
    public void beforeDeleteAccept() throws Exception {
        String input = readResource("/entities/beforedelete/beforedelete-accept.json");
        executor.executeAction(action, LifeCycleHook.beforeDelete, reltioAPI, input);
    }
}