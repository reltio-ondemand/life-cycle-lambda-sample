package com.reltio.lca.entities;

import com.reltio.lca.BaseTest;
import com.reltio.lca.exceptions.CountryNotMatchingException;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.test.LifecycleExecutor;
import com.reltio.microservices.lcaservice.service.services.LifeCycleHook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Map;

public class BeforeMergeEntityActionTest extends BaseTest {

    private final LifecycleExecutor actionExecutor = new LifecycleExecutor();
    private final BeforeMergeEntityAction action = new BeforeMergeEntityAction();
    private IReltioAPI reltioAPI;

    @BeforeEach
    public void setupMocks() {
        reltioAPI = mockReltioAPI();
    }

    @Test
    public void beforeMergeDifferentCountry() throws Exception {
        String input = readResource("/entities/beforemerge/byuri/test-reject-before-merge.json");
        String entitiesResponse = readResource("/entities/beforemerge/byuri/entities-response-before-merge.json");
        Mockito.when(reltioAPI.post(Mockito.anyString(), Mockito.anyString(), Mockito.any(Map.class))).thenReturn(entitiesResponse);
        Assertions.assertThrows(CountryNotMatchingException.class, () -> actionExecutor.executeAction(action, LifeCycleHook.beforeMerge, reltioAPI, input));
    }

    @Test
    public void beforeMergeSameCountry() throws Exception {
        String input = readResource("/entities/beforemerge/byuri/test-accepts-before-merge.json");
        String entitiesResponse = readResource("/entities/beforemerge/byuri/entities-response-before-merge.json");
        Mockito.when(reltioAPI.post(Mockito.anyString(), Mockito.anyString(), Mockito.any(Map.class))).thenReturn(entitiesResponse);
        actionExecutor.executeAction(action, LifeCycleHook.beforeMerge, reltioAPI, input);
    }
}