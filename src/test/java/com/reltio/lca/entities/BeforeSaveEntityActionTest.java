package com.reltio.lca.entities;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.reltio.lca.BaseTest;
import com.reltio.lifecycle.test.LifecycleExecutor;
import com.reltio.microservices.lcaservice.service.services.LifeCycleHook;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class BeforeSaveEntityActionTest extends BaseTest {

    private final LifecycleExecutor executor = new LifecycleExecutor();
    private final BeforeSaveEntityAction handler = new BeforeSaveEntityAction();
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void testFullNameMissing() throws Exception {
        String input = readResource("/entities/beforesave/input.json");
        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        String name = jsonNode.get("object").get("attributes").get("Name").elements().next().get("value").asText();
        assertEquals("John Snow", name);
    }

    @Test
    public void testWithExistingFullName() throws Exception {
        String input = readResource("/entities/beforesave/input_with_full_name.json");
        String result = executor.executeAction(handler, LifeCycleHook.beforeSave, input);
        JsonNode jsonNode = objectMapper.readValue(result, JsonNode.class);
        assertNull(jsonNode.get("object"));
    }
}
