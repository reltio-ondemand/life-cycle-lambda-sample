package com.reltio.lca.entities;

import com.reltio.lca.BaseTest;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.test.LifecycleExecutor;
import com.reltio.microservices.lcaservice.service.services.LifeCycleHook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BeforeReferenceAttributeAddedEntityActionTest extends BaseTest {

    private final LifecycleExecutor actionExecutor = new LifecycleExecutor();
    private final BeforeReferenceAttributeAddedEntityAction action = new BeforeReferenceAttributeAddedEntityAction();

    private IReltioAPI reltioAPI;

    @BeforeEach
    public void setupMocks() {
        reltioAPI = mockReltioAPI();
    }

    @Test
    public void beforeReferenceAttributeAddedWithoutAddressType() throws Exception {
        String input = readResource("/entities/beforeReferenceAttributeAdded/input-without-addresstype.json");
        String output = actionExecutor.executeAction(action, LifeCycleHook.beforeReferenceAttributeAdded, reltioAPI, input);
        Assertions.assertNotNull(output);
    }
}