package com.reltio.lca.entities;

import com.reltio.lca.BaseTest;
import com.reltio.lca.entities.ValidateEntityAction;
import com.reltio.lifecycle.test.LifecycleExecutor;
import com.reltio.microservices.lcaservice.service.services.LifeCycleHook;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class ValidateEntityActionTest extends BaseTest {

    private final ValidateEntityAction action = new ValidateEntityAction();
    private final LifecycleExecutor executor = new LifecycleExecutor();

    @Test
    void validate() throws Exception {
        String input = readResource("/entities/validate/validate-name-fail.json");
        Map<String, Object> result = executor.executeActionForMap(action, LifeCycleHook.validate, input);
        List<Map<String, String>> errors = (List<Map<String, String>>) result.get("validationErrors");
        assertNotNull(errors);
        assertEquals(2, errors.size());
        List<String> errorUris = errors.stream().map(error -> error.get("objectTypeUri")).collect(Collectors.toList());
        assertTrue(errorUris.contains("configuration/entityTypes/HCP/attributes/LastName"));
        assertTrue(errorUris.contains("configuration/entityTypes/HCP/attributes/FirstName"));
    }
}