package com.reltio.lca.entities;

import com.reltio.lca.BaseTest;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.test.LifecycleExecutor;
import com.reltio.microservices.lcaservice.service.services.LifeCycleHook;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class BeforeSaveAddUUIDWithExistingCrosswalkEntityActionTest extends BaseTest {

    private final LifecycleExecutor actionExecutor = new LifecycleExecutor();
    private BeforeSaveAddUUIDWithExistingCrosswalkEntityAction action;
    private IReltioAPI reltioAPI;

    @BeforeEach
    public void setupMocks() {
        reltioAPI = mockReltioAPI();
        action = new BeforeSaveAddUUIDWithExistingCrosswalkEntityAction();
    }

    @Test
    public void beforeSave() throws Exception {
        String input = readResource("/entities/beforesave/withexistingcrosswalk/input.json");
        Map<String, Object> beforeEntity = jsontoMap(input);
        int existingCrosswalks = ((List) ((Map) beforeEntity.get("object")).get("crosswalks")).size();
        String output = actionExecutor.executeAction(action, LifeCycleHook.beforeSave, reltioAPI, input);
        assertNotNull(output);
        Map<String, Object> entityMap = jsontoMap(output);
        List<Map> uniqueIds = (List<Map>) ((Map) ((Map) entityMap.get("object")).get("attributes")).get("UniqueID");
        assertNotNull(uniqueIds);
        assertNotNull(uniqueIds.get(0).get("value"));
        List<Map> crosswalks = (List<Map>) ((Map) entityMap.get("object")).get("crosswalks");
        Optional<Map> optionalCrosswalk = crosswalks.stream().filter(crosswalk -> "configuration/sources/Unique".equals(crosswalk.get("type"))).findFirst();
        assertFalse(optionalCrosswalk.isPresent());
        assertEquals(existingCrosswalks, crosswalks.size());
    }
}