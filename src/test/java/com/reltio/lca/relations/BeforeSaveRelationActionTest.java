package com.reltio.lca.relations;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.reltio.lca.BaseTest;
import com.reltio.lifecycle.framework.IObject;
import com.reltio.lifecycle.framework.IReltioAPI;
import com.reltio.lifecycle.server.core.framework.data.LifeCycleObjectData;
import com.reltio.lifecycle.test.LifecycleExecutor;
import com.reltio.microservices.lcaservice.service.services.LifeCycleHook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class BeforeSaveRelationActionTest extends BaseTest {

    private final LifecycleExecutor executor = new LifecycleExecutor();
    private final ObjectMapper objectMapper = new ObjectMapper();
    private BeforeSaveRelationAction action;
    private IReltioAPI reltioAPI;

    @BeforeEach
    public void setupMocks() {
        reltioAPI = mockReltioAPI();
        action = new BeforeSaveRelationAction();
    }

    @Test
    public void testDefaultAddressType() throws Exception {
        String input = readResource("/relations/beforesave/attributeAddressTypeNonExistinput.json");
        String result = executor.executeAction(action, LifeCycleHook.beforeSave, reltioAPI, input);
        Map inputData = this.objectMapper.readValue(result, Map.class);
        IObject entity = new LifeCycleObjectData(reltioAPI, inputData).getObject();
        assertEquals("Physical", entity.getAttributes().getAttributeValues("AddressType").get(0).getValue());
        Assertions.assertNotNull(inputData.get("object"));
    }

    @Test
    public void testAddressTypeExist() throws Exception {
        String input = readResource("/relations/beforesave/attributeAddressTypeExistinput.json");
        String result = executor.executeAction(action, LifeCycleHook.beforeSave, reltioAPI, input);
        Map inputData = this.objectMapper.readValue(result, Map.class);
        assertNull(inputData.get("object"));
    }
}