package com.reltio.lca;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.reltio.lifecycle.framework.IReltioAPI;
import org.mockito.Mockito;
import org.mockito.stubbing.Answer;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;

public abstract class BaseTest {

    private final ObjectMapper mapper = new ObjectMapper();

    protected String readResource(String path) throws Exception {
        return new String(Files.readAllBytes(Paths.get(getClass().getResource(path).toURI())));
    }

    public IReltioAPI mockReltioAPI() {
        IReltioAPI api = Mockito.mock(IReltioAPI.class);
        final Answer logAnswer = invocation -> {
            System.out.println(invocation.getArguments()[0]);
            return null;
        };
        Mockito.doAnswer(logAnswer).when(api).logError(Mockito.anyString());
        Mockito.doAnswer(logAnswer).when(api).logWarning(Mockito.anyString());
        Mockito.doAnswer(logAnswer).when(api).logInfo(Mockito.anyString());
        return api;
    }

    public Map<String,Object> jsontoMap(String json) throws Exception {
        return mapper.readValue(json, Map.class);
    }
}
